# Simple sample CRUD API by J. Akbar (jadiakbar@gmail.com)
1. Node JS
2. Express
3. Mongoose DB
4. Uuid
5. JWT

A. Install & Run
>npm install
>npm start
# open browser http://localhost:4001 (can be configured on .env)


B. Testing on Postman
a. REGISTER, method: POST
URL: ../register
{
    "userName": "yourname",
    "accountNumber": "2",
    "emailAddress": "email@localhost.com",
    "identityNumber": "2",
    "password": "12345"
}

b. LOGIN, method: POST
URL: ../login
{
    "emailAddress": "email@localhost.com",
    "password": "12345"
}
(*response body after login) :
{
    "userName": "yourname",
    "accountNumber": "2",
    "emailAddress": "email@localhost.com",
    "identityNumber": "2",
    "_id": "6182feb4210dc82c4c2beef3",
    "password": "$2a$10$EUzHQxIjaxLfvxOSzOZfmOs4epf8hLDLxnHFXZfahIVwv59YvymTe",
    "__v": 0,
    "Id": "e43e2b50-3e53-11ec-b6d5-c1e43fd7505a",
    "token": "your token"
}
(copy token for next step...)

c. UPDATE, method: POST
URL: ../update
set headers : 
[{"key":"x-access-token","value":"your token","description":"","type":"text","enabled":true}]
{
    "userName": "yourname",
    "accountNumber": "1",
    "emailAddress": "email@localhost.com",
    "identityNumber": "1",
    "password": "12345"
}

d. DELETE, method: DELETE
set headers : 
[{"key":"x-access-token","value":"your token","description":"","type":"text","enabled":true}]
URL: ../users/your token

e. GET ALL USERS, method: GET
set headers : 
[{"key":"x-access-token","value":"your token","description":"","type":"text","enabled":true}]
URL: ../users

f. GET USER BY ACCOUNT NUMBER, method: GET
set headers : 
[{"key":"x-access-token","value":"your token","description":"","type":"text","enabled":true}]
URL: ../users/accountNumber/2

g. GET USER BY IDENTITY NUMBER, method: GET
set headers : 
[{"key":"x-access-token","value":"your token","description":"","type":"text","enabled":true}]
URL: ../users/identityNumber/2
set headers : 
[{"key":"x-access-token","value":"your token","description":"","type":"text","enabled":true}]
