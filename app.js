require("dotenv").config();
require("./config/database").connect();
const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const uuid = require('uuid');
const ObjectID = require('mongodb');
var Todo = require("./model/todo");
const User = require("./model/user");
const auth = require("./middleware/auth");
const redis = require('redis');

/* create and connect redis client to local instance.
const redis_akbar_betest = redis.createClient(6380, "localhost");
// Print redis errors to the console
redis_akbar_betest.on('error', (err) => {
  console.log("Error " + err);
}); */

const app = express();

app.use(express.json({ limit: "50mb" }));

// CREATE
app.post("/register", async (req, res) => {
  try {
    // Get user input
    const { userName, accountNumber, emailAddress, identityNumber, password } = req.body;

    // Validate user input
    if (!(userName && accountNumber && emailAddress && identityNumber)) {
      res.status(400).send("All input is required");
    }

    // check if user already exist
    // Validate if user exist in our database
    const oldUser = await User.findOne({ emailAddress });

    if (oldUser) {
      return res.status(409).send("User Already Exist. Please Login");
    }

    //Encrypt user password
    encryptedPassword = await bcrypt.hash(password, 10);

    // Create user in our database
    const user = await User.create({
      userName,
      accountNumber,
      emailAddress: emailAddress.toLowerCase(), // sanitize: convert email to lowercase
      identityNumber,
      password: encryptedPassword,
    });

    // Create Id (time-based) id
    const Id = uuid.v1();
    // save user Id
    user.Id = Id;

    // Create token
    const token = jwt.sign(
      { Id: user.Id, emailAddress },
      process.env.TOKEN_KEY,
      {
        expiresIn: "2h",
      }
    );
    // save user token
    user.token = token;

    // return new user
    res.status(201).json(user);
  } catch (err) {
    console.log(err);
  }
});

// UPDATE
app.post("/update", async (req, res) => {
  try {
    // Get user input
    const { userName, accountNumber, emailAddress, identityNumber, password } = req.body;

    // Validate user input
    if (!(userName && accountNumber && emailAddress && identityNumber)) {
      res.status(400).send("All input is required");
    }

    //Encrypt user password
    encryptedPassword = await bcrypt.hash(password, 10);

    // Create user in our database
    const user = await User.updateOne({
      userName,
      accountNumber,
      emailAddress: emailAddress.toLowerCase(), // sanitize: convert email to lowercase
      identityNumber,
      password: encryptedPassword,
    });

    // Create Id (time-based) id
    const Id = uuid.v1();
    // save user Id
    user.Id = Id;

    // Create token
    const token = jwt.sign(
      { Id: user.Id, emailAddress },
      process.env.TOKEN_KEY,
      {
        expiresIn: "2h",
      }
    );
    // save user token
    user.token = token;

    // return new user
    res.status(201).json(user);
  } catch (err) {
    console.log(err);
  }
});


// GET ALL USER
app.get('/users', auth, (req, res) => {
  /* return redis_akbar_betest.get(`wikipedia:${query}`, (err, result) => {
    User.find({
      _creator: req.user._id
    }).then((user) => {
      res.send({user});
    }).catch((err) => {
      res.status(400).send(err);
    });
  }); */
  User.find({
    _creator: req.user._id
  }).then((user) => {
    res.send({user});
  }).catch((err) => {
    res.status(400).send(err);
  });
});

// get user by accountNumber
app.get('/users/accountNumber/:accountNumber', auth, (req, res) => {
  var accountNumber = req.params.accountNumber;
  console.log("accountNumber:"+accountNumber);

  User.findOne({
    accountNumber: accountNumber
  }).then((user) => {
    if(!user) return res.status(404).send();

    res.send({user});
  }).catch((err) => {
    res.status(400).send();
  });
});

// get user by identityNumber
app.get('/users/identityNumber/:identityNumber', auth, (req, res) => {
  var identityNumber = req.params.identityNumber;
  console.log("identityNumber:"+identityNumber);

  User.findOne({
    identityNumber: identityNumber
  }).then((user) => {
    if(!user) return res.status(404).send();

    res.send({user});
  }).catch((err) => {
    res.status(400).send();
  });
});

// DELETE
app.delete('/users/:id', auth, (req, res) => {
  var id = req.user.id;
  console.log("ini id:"+id);

  User.findOneAndRemove({
    id: id
  }).then((user) => {
    if(!user) return res.status(404).send();

    res.send({user});
  }).catch((err) => {
    return res.status(400).send();
  })

}); 

// DELETE ALL USERS
app.delete('/users/me/token', auth, (req, res) => {
  req.user.removeToken(req.token).then(() => {
    res.status(200).send()
  }).catch((err) => {
    res.status(400).send(err);
  });
});

app.post("/login", async (req, res) => {
  try {
    // Get user input
    const { emailAddress, password } = req.body;

    // Validate user input
    if (!(emailAddress && password)) {
      res.status(400).send("All input is required");
    }
    // Validate if user exist in our database
    const user = await User.findOne({ emailAddress });

    if (user && (await bcrypt.compare(password, user.password))) {

      // Create Id (time-based) id
      const Id = uuid.v1();
      // save user Id
      user.Id = Id;
      
      // Create token
      const token = jwt.sign(
        { Id: user.Id, emailAddress },
        process.env.TOKEN_KEY,
        {
          expiresIn: "2h",
        }
      );

      // save user token
      user.token = token;

      // user
      res.status(200).json(user);
    }
    res.status(400).send("Invalid Credentials");
  } catch (err) {
    console.log(err);
  }
});

app.get("/welcome", auth, (req, res) => {
  res.status(200).send("Welcome User 🙌 ");
});

// This should be the last route else any after it won't work
app.use("*", (req, res) => {
  res.status(404).json({
    success: "false",
    message: "Page not found",
    error: {
      statusCode: 404,
      message: "You reached a route that is not defined on this server",
    },
  });
});

module.exports = app;
