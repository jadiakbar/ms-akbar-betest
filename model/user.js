const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  Id: { type: String, unique: true},
  userName: { type: String, default: null },
  accountNumber: { type: String, default: null },
  //emailAddress: { type: String, unique: true },
  emailAddress: { type: String, default: null },
  identityNumber: { type: String, default: null },
  password: { type: String },
  token: { type: String },
});

module.exports = mongoose.model("user", userSchema);
