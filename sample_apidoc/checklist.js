/**
 * @api {get} /checklists/{checklistId} Get Checklist
 * @apiName Get Checklist
 * @apiGroup Checklist
 *
 * @apiHeader Authorization Basic Access Authentication token.
 * @apiHeader Content-Type (application/x-www-form-urlencoded, application/json, application/xml).
 *
 * @apiParam {String} checklistid Filter of the Checklist.
 * @apiParam {String} [include] To include all items of this checklist use “include=items” in query string. The value accepted only “items”.
 * @apiParam {String} [filter] Filter sort of the Checklist.
 * @apiParam {String} [sort] Filter sort of the Checklist.
 * @apiParam {String} [fields] Filter field of the Checklist.
 * @apiParam {String} [page_limit] Filter limit return source of the Checklist.
 * @apiParam {String} [page_offset] Filter page limit return source of the Checklist.
 *
 * @apiParamExample Success-Response:
 *     HTTP/1.1 200 OK
        {
            "data": {
                "type": "checklists",
                "id": 1,
                "attributes": {
                "object_domain": "contact",
                "object_id": "1",
                "description": "Need to verify this guy house.",
                "is_completed": false,
                "due": null,
                "urgency": 0,
                "completed_at": null,
                "last_update_by": null,
                "update_at": null,
                "created_at": "2018-01-25T07:50:14+00:00"
                },
                "links": {
                "self": "https://dev-kong.command-api.kw.com/checklists/50127"
                }
            }
        }
 *
 * @apiError ChecklistNotFound The filter of the Checklist was not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
        "status": "404",
        "error": "Not Found"
       }
 */