/**
 * @api {get} /checklists/templates List all checklists templates
 * @apiName List all checklists templates
 * @apiGroup Templates
 * 
 * @apiHeader Authorization Basic Access Authentication token.
 * @apiHeader Content-Type (application/x-www-form-urlencoded, application/json, application/xml).
 *
 * @apiParam {String} [filter] Filter of the Templates.
 * @apiParam {String} [sort] Filter sort of the Templates.
 * @apiParam {String} [page_limit] Filter limit return source of the Templates.
 * @apiParam {String} [page_offset] Filter page limit return source of the Templates.
 *
 * @apiParamExample Success-Response:
 *     HTTP/1.1 200 OK
        {
        "meta": {
            "count": 10,
            "total": 100
        },
        "links": {
            "first": "https://kong.command-api.kw.com/api/v1/checklists/templates?page[limit]=10&page[offset]=0",
            "last": "https://kong.command-api.kw.com/api/v1/checklists/templates?page[limit]=10&page[offset]=10",
            "next": "https://kong.command-api.kw.com/api/v1/checklists/templates?page[limit]=10&page[offset]=10",
            "prev": "null"
        },
        "data": [
            {
            "name": "foo template",
            "checklist": {
                "description": "my checklist",
                "due_interval": 3,
                "due_unit": "hour"
            },
            "items": [
                {
                "description": "my foo item",
                "urgency": 2,
                "due_interval": 40,
                "due_unit": "minute"
                },
                {
                "description": "my bar item",
                "urgency": 3,
                "due_interval": 30,
                "due_unit": "minute"
                }
            ]
            }
        ]
        }
 *
 * @apiError Unauthorized The Authorized not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Unauthorized"
 *     }
 */